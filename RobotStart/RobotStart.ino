#include <MeetAndroid.h>

#include <AFMotor.h>

MeetAndroid meetAndroid;


String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int SpeedLeft =0;
int SpeedRight =0;

AF_DCMotor motorR(1);
AF_DCMotor motorL(4);

void setup() {
  Serial.begin(9600);           // set up Serial library at 9600 bps

  // register callback functions, which will be called when an associated event occurs.
  // - the first parameter is the name of your function (see below)
  // - match the second parameter ('A', 'B', 'a', etc...) with the flag on your Android application
  meetAndroid.registerFunction(updateSpeedLeft, 'L');
  meetAndroid.registerFunction(updateSpeedRight, 'R');
  meetAndroid.registerFunction(updateDirection, 'd');


//pins for distance sensor 
pinMode(3, OUTPUT);// attach pin 3 to Trig
 pinMode (4, INPUT);//attach pin 4 to Echo
  
  Serial.println("motorR test!");
  inputString.reserve(200);
// establish variables for duration of the ping,
  // and the distance result in inches and centimeters:
  

}

void loop() {
  //variable for distance sensor
  long duration, cm;
    // Check for new BT data
  meetAndroid.receive();
  
  uint8_t i;

    
motorR.run(FORWARD);
    motorL.run(FORWARD);
      motorR.setSpeed(SpeedLeft);
      motorL.setSpeed(SpeedRight);  
      delay(10);
      
      
  // get distance from sensor

  // The PING))) is triggered by a HIGH pulse of 2 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  
  digitalWrite(3, LOW);
  delayMicroseconds(2);
  digitalWrite(3, HIGH);
  delayMicroseconds(5);
  digitalWrite(3, LOW);

  // The same pin is used to read the signal from the PING))): a HIGH
  // pulse whose duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  duration = pulseIn(4, HIGH);  
  cm = microsecondsToCentimeters(duration);
  
        // read input pin and send result to Android
  meetAndroid.send(cm);
  
  // add a little delay otherwise the phone is pretty busy
  delay(100);
    
  
 }
 



//*************************************************************************************************
// MeetAndroid methods


void updateSpeedLeft(byte flag, byte numOfValues)
{
  int data = meetAndroid.getInt();
SpeedLeft = data;
}

void updateSpeedRight(byte flag, byte numOfValues)
{
  int data = meetAndroid.getInt();
SpeedRight = data;
}

void updateDirection(byte flag, byte numOfValues)
{
  int data = meetAndroid.getInt();
SpeedRight = 100;
}



long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}
