package com.example.robotcontrol;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.Toast;
import at.abraxas.amarino.Amarino;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
	    final SeekBar Speed=(SeekBar)findViewById(R.id.Speed);
        Speed.setOnSeekBarChangeListener(new yourListener());
        
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

class yourListener extends ActionBarActivity implements SeekBar.OnSeekBarChangeListener {
    public static final String DEVICE_ADDRESS = "00:13:12:06:62:05";

    public void onCreate() {

        Amarino.connect(this, DEVICE_ADDRESS);
        Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'p', 0);

        // this is how you tell Amarino to connect to a specific BT device from within your own code
        Amarino.connect(this, DEVICE_ADDRESS);

    }

    public void onStart() {
        Amarino.connect(this, DEVICE_ADDRESS);
        Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'p', 200);

    }


    public void onStop() {
        super.onStop();
        Amarino.disconnect(this, DEVICE_ADDRESS);
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
      //Amarino.sendDataToArduino(this, DEVICE_ADDRESS, 'p', 50);


    }

    public void onStartTrackingTouch(SeekBar seekBar) {}

    public void onStopTrackingTouch(SeekBar Speed) {
        Toast.makeText(getApplicationContext(), "this is my Toast message!!! =)", Toast.LENGTH_LONG).show();
    }

}
